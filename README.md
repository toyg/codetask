Interview coding task
======

Repository structure
------

/docs - documentation files

/myclient - production code

/tests - pytest fixtures

Setup
-----

Tested with Python 3.9.4, but should work with 3.7+.

Both `myclient` and `tests` include a requirements file for pip, 
with libraries pinned to a specific version for reliability.
We could have used `poetry` or `pipenv` but it would have probably been overkill.

```shell
pip install -r myclient/requirements.txt
pip install -r tests/requirements-tests.txt
```

You will need an .env file added to the root, pointing to your environment.
Here's the minimum info required:
```text
MGMT_URL=your-server
MGMT_IP=your-ip
MGMT_USER=your-user
MGMT_PWD=your-pwd

WRK1_URL=worker-1
WRK1_IP=wk1-ip

WRK2_URL=wk2
WRK2_IP=wk2-ip
```


Test
-----
The project uses pytest and pytest-vcr, which means tests can be run 
multiple times without stressing servers.

A simple script is provided to wrap pytest innards:
```shell
./test.sh
```

Run
-----
An entry point is provided to run all tasks in one go.
In real environments we would obviously split this into multiple 
parameterized scripts.

```shell
python tasks.py
```
