from myclient import RoomManager, InfraManager, LicenseManager, SchemaManager
from myclient.RoomManager import RoomDoesNotExist

if __name__ == '__main__':
    # Create a Virtual Meeting Room called vmr_test.
    roomMgr = RoomManager.RoomManager()
    roomMgr.create_room(name="vmr_test")
    print("Task 1 done")

    # Print all available licensed features and the total count for
    # all active licences. (E.g.: audio: 100, video: 250)
    licMgr = LicenseManager.LicenseManager()
    totals = licMgr.get_totals()
    print("Licenses:")
    for t, v in totals.items():
        print(f"- {t[:-6]}: {v}")
    print("Task 3 done")

    # Create a Virtual Meeting Room called vmr_test2
    # if it does not exist, but if the VMR already exists then delete it.
    roomName = 'vmr_test2'
    try:
        vmr2 = roomMgr.get_room(name=roomName)
        roomMgr.delete_room(vmr2)
    except RoomDoesNotExist as rdne:
        vmr2 = roomMgr.create_room(name=roomName)
    print("Task 4 done")

    # Print all allowed HTTP methods and all available fields for the DNS Server resource.
    schMgr = SchemaManager.SchemaManager()
    feats = schMgr.get_schema('/configuration/v1/dns_server')
    print("Detail http methods for dns: ")
    for m in feats['allowed_detail_http_methods']:
        print(f"- {m}")

    print("List http methods for dns: ")
    for m in feats['allowed_list_http_methods']:
        print(f"- {m}")

    print("Fields for dns:")
    for f in feats['fields']:
        print(f"- {f}")

    print("Done task 5")

    # this is broken
    # Add 8.8.4.4 as a DNS Server and configure "location 1" to use it.
    test_ip = '8.8.4.4'
    location_id = 1
    infraMgr = InfraManager.InfraManager()
    dns = infraMgr.create_dns(test_ip)
    available_dns = infraMgr.get_all_dns()
    infraMgr.enable_dns(location_id, dns)

    print("Done task 6")