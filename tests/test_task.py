# tests to satisfy tasks
import pytest

from myclient.InfraManager import InfraManager
from myclient.LicenseManager import LicenseManager
from myclient.RoomManager import RoomManager, RoomDoesNotExist
from myclient.SchemaManager import SchemaManager


@pytest.mark.vcr
def test_create_vmr():
    # Create a Virtual Meeting Room called vmr_test.
    roomMgr = RoomManager()
    vmr_test_room = roomMgr.create_room(name='vmr_test')
    assert roomMgr.room_exists(vmr_test_room)
    assert roomMgr.delete_room(vmr_test_room)

@pytest.mark.vcr
def test_create_or_delete_vmr():
    # Create a Virtual Meeting Room called vmr_test2 if it does not exist,
    # but if the VMR already exists then delete it.
    roomName = 'vmr_test2'
    roomMgr = RoomManager()
    try:
        vmr2 = roomMgr.get_room(name=roomName)
        assert roomMgr.delete_room(vmr2)
        assert not roomMgr.room_exists(vmr2)
    except RoomDoesNotExist as rdne:
        vmr2 = roomMgr.create_room(name=roomName)
        assert roomMgr.room_exists(vmr2)
        roomMgr.delete_room(vmr2)

@pytest.mark.vcr
def test_dns():
    # Add 8.8.4.4 as a DNS Server and configure "location 1" to use it.
    test_ip = '8.8.4.4'
    location_id = 1
    infraMgr = InfraManager()
    dns = infraMgr.create_dns(test_ip)
    available_dns = infraMgr.get_all_dns()
    found = False
    for server in available_dns:
        if server.ip == test_ip:
            found = True
    assert found
    assert infraMgr.enable_dns(location_id, dns)
    location_details = infraMgr.get_location(location_id)
    found = False
    for server in location_details['dns_servers']:
        if server['address'] == test_ip:
            found = True
    assert found
    infraMgr.disable_dns(location_id, dns)
    infraMgr.delete_dns(dns)

@pytest.mark.vcr
def test_licenses():
    # Print all available licensed features and the total count for all active
    # licences. (E.g.: audio: 100, video: 250)
    licMgr = LicenseManager()
    licenses = licMgr.get_totals()
    assert len(licenses) > 0
    assert type(licenses) == dict

@pytest.mark.vcr
def print_methods():
    # Print all allowed HTTP methods and all available fields
    # for the DNS Server resource.
    schMgr = SchemaManager()
    details = schMgr.get_feature('dns_server')
    assert type(details) == dict
    assert 'allowed_detail_http_methods' in details
    assert 'allowed_list_http_methods' in details
    assert 'fields' in details