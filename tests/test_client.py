import pytest

from myclient import ApiClient

@pytest.mark.vcr()
def test_client():
    client = ApiClient.build_client()
    result = client.call('/configuration/v1/licence/', 'GET')
    assert result.status_code == 200
    assert result.headers.get('Content-Type') == 'application/json'