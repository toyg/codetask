from .ApiClient import build_client

class SchemaManager:
    """
    Schema documentation utils
    """


    supported_features = [
        '/configuration/v1/dns_server'
    ]

    def __init__(self):
        self._client = build_client()

    def get_schema(self, feature:str):

        if feature not in self.supported_features:
            raise Exception("Sorry, I don't support that feature")

        result = self._client.call(f"{feature}/schema/", 'GET')
        if result.status_code == 200:
            # we assume it's json, and otherwise we bomb out to force review
            return result.json()
