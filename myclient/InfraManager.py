from dataclasses import dataclass

from .ApiClient import build_client


def is_valid_ip(ip):
    # here we can drop our checks
    return True


@dataclass
class Dns:
    ip: str = ""
    id: int = 0
    desc: str = ""


class InfraManager:
    """
    Class dedicated to generic infrastructure management
    """

    def __init__(self):
        self._client = build_client()

    def get_location(self, location_id:int):
        """
        Retrieve details about locations
        :param location_id: int
        :return: dict with location properties
        """
        result = self._client.call(
            f'/configuration/v1/system_location/{location_id}', 'GET')
        if not result.status_code == 200:
            raise Exception(f"Unexpected status from location retrieval:\n\n "
                            f"{result.status_code} {result.content}")
        return result.json()

    def create_dns(self, ip, description: str = None):
        """
        Define a dns server
        :param ip: str ip address
        :param description: str optional description
        :return:
        """
        if not is_valid_ip(ip):
            raise Exception(f"invalid IP passed to create_dns: {ip}")
        desc = description or "created via API"
        result = self._client.call('/configuration/v1/dns_server/', 'POST',
                                   {'address': ip,
                                    'description': desc})
        if result.status_code == 201:
            # purge /api/admin from url
            result2 = self._client.call(result.headers['location'][10:], 'GET')
            if result2.status_code == 200:
                props = result2.json()
                return Dns(ip=props['address'], id=int(props['id']),
                           desc=props['description'])
            else:
                raise Exception(f"Unexpected status from dns details:\n\n "
                                f"{result2.status_code} {result2.content}")
        else:
            raise Exception(f"Unexpected status from dns creation:\n\n "
                            f"{result.status_code} {result.content}")

    def delete_dns(self, dns: Dns):
        """
        Delete a dns server config.
        :param dns: Dns object to delete
        :return: True on success
        """

        # first get all locations to check if in use
        result = self._client.call(f'/configuration/v1/system_location', 'GET')
        if not result.status_code == 200:
            raise Exception(f"failed retrieving locations: \n\n"
                            f"{result.status_code} {result.content}")
        using = []
        for server in result.json()['objects']:
            for dnsServer in server['dns_servers']:
                if dnsServer['address'] == dns.ip:
                    using.append(server)
                    break

        # then disable it on each location
        for location in using:
            self.disable_dns(int(location['id']), dns)

        # then delete it
        result = self._client.call(f'/configuration/v1/dns_server/{dns.id}',
                                   'DELETE')
        if not result.status_code == 204:
            raise Exception(f"dns deletion failed: \n\n"
                            f"{result.status_code} {result.content}")
        return True

    def get_all_dns(self):
        """
        retrieve all available dns servers
        :return: list of Dns objects
        """
        result = self._client.call('/configuration/v1/dns_server/', 'GET')
        if result.status_code == 200 and 'objects' in result.json():
            return [Dns(ip=s['address'], id=s['id'], desc=s['description'])
                    for s in result.json()['objects']]

    def enable_dns(self, location_id: int, dns: Dns):
        """ enable dns for a given location.
        CURRENTLY BROKEN
        """
        # retrieve current config
        loc = self.get_location(location_id)
        servers = [f"/api/admin/configuration/v1/dns_server/{s['id']}/"
                   for s in loc['dns_servers']]
        servers.append(f"/api/admin/configuration/v1/dns_server/{dns.id}/")
        print(servers)
        result = self._client.call(
            f'/configuration/v1/system_location/{location_id}', 'PATCH',
            {'dns_servers': servers})
        if not result.status_code == 200:
            raise Exception(f"dns enablement failed: \n\n"
                            f"{result.status_code} {result.content}")
        return True

    def disable_dns(self, location_id: int, dns: Dns):
        """
        Disable DNS for location
        :param location_id: int
        :param dns: Dns to disable
        :return: True on success
        """
        loc = self.get_location(location_id)
        servers = loc['dns_servers']
        new_servers = [s for s in servers if s['address'] != dns.ip]
        result = self._client.call(
            f'/configuration/v1/system_location/{location_id}', 'PATCH',
            {'dns_servers': new_servers})
        if not result.status_code == 200:
            print(result.status_code)
            raise Exception(f"dns enablement failed: \n\n"
                            f"{result.status_code} {result.content}")
        return True
