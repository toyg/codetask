import logging
import os
from pathlib import Path

from dotenv import load_dotenv
from requests import Session

# env setup
load_dotenv()
MGMT_URL = os.getenv('MGMT_URL')
MGMT_USER = os.getenv('MGMT_USER')
LOG_LEVEL = logging.getLevelName(os.getenv('LOGLEVEL', 'DEBUG'))

# logging
logging.basicConfig(
    filename=Path.home().joinpath('mycompany_apiclient_log.txt'),
    filemode='w', level=LOG_LEVEL)

# constants
URL_BASE = f"https://{MGMT_URL}/api/admin"
URL_SCHEMA_SUFFIX = "schema/?format=html"


def _retrieve_password(username: str = None):
    """
    Abstracting password retrieval is typically a good idea,
    from a security standpoint.
    We can then store it in secure places like system wallet,
    encrypted files, etc.
    :param username: user to which password is associated
    :return: password value
    """
    if not username:
        raise Exception("When you ask for a password, "
                        "you need to provide a username")
    # ... in practice at the moment it's just a stub
    pwd = os.getenv('MGMT_PWD', None)
    if not pwd:
        raise Exception(f"No password for user {username} ")
    return pwd


class ApiClient:
    def __init__(self, mgmt_url: str = None, mgmt_user: str = None):
        """
        Init client
        :param mgmt_url: url of management server
        :param mgmt_user: username of admin
        """
        self._mgmt_url = mgmt_url
        self._mgmt_user = mgmt_user
        self.__session = Session()
        # slightly naughty to keep security stuff around, but python doesn't
        # really have the facilities of, say, Java, to deal with that sort of
        # lifecycle, so whatever
        self.__session.auth = (
        self._mgmt_user, _retrieve_password(self._mgmt_user))

    def call(self, api_target: str, method: str = None,
              api_params: dict = None, **kwargs):
        """
        Execute api call.
        This slight level of indirection would allow us to set additional
        stuff in requests if necessary (custom headers, etc)

        :param api_target: api target fragment, i.e. /configuration/v1/dns_server
        :param method: HTTP verb
        :param params: dict of parameters to submit
        :param kwargs: extra parameters to percolate directly to Requests
        :return: Response object
        """

        url = f"{URL_BASE}{api_target}"
        try:
            return self.__session.request(method, url,
                                          json=api_params,
                                          **kwargs)
        except Exception as e:
            logging.error(
                f"Error executing api call to {URL_BASE}{api_target}\n", e)
            return None

def build_client():
    """ utility for downstream use """
    return ApiClient(mgmt_url=MGMT_URL, mgmt_user=MGMT_USER)