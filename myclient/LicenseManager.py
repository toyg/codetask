from .ApiClient import build_client


class LicenseManager:

    def __init__(self):
        self._client = build_client()

    def get_totals(self):
        """Return all license totals"""
        result = self._client.call('/status/v1/licensing', 'GET')
        if not result.status_code == 200 or \
                not ('objects' in result.json()) or \
                len(result.json()['objects']) < 1:
            raise Exception(f"Unexpected result from licensing api:\n\n"
                            "{result.status_code} {result.content}")
        return {key: value for (key, value)
                in result.json()['objects'][0].items()
                if key.endswith('_total')}
