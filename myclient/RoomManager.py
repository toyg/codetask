from dataclasses import dataclass

from .ApiClient import build_client


@dataclass
class Room:
    name: str = ""
    id: int = 0
    props: dict = None


class RoomDoesNotExist(Exception):
    pass


class RoomManager:
    """
    Room management
    """

    def __init__(self):
        self._client = build_client()

    def create_room(self, name=None):
        """
        Create a room.
        :param name: str optional name for the room.
        :return: valid Room object on success
        """
        roomName = name or ''
        result = self._client.call('/configuration/v1/conference/', 'POST',
                                   {'name': roomName,
                                    'service_type': 'conference'})
        # this api seems to return 201 if you pass parameters, and 200 if not
        if result.status_code == 201:
            # remove /api/admin from call
            result2 = self._client.call(result.headers['location'][10:], 'GET')
            roomProps = result2.json()
            return Room(name=roomName,
                        props=roomProps,
                        id=int(roomProps['id']))
        else:
            raise Exception(f"Error creating room. Response was:\n\n"
                            f"{result.status_code} {result.content}")
        return None

    def delete_room(self, room_obj: Room = None):
        """
        Delete a room
        :param room_obj: Room object to delete
        :return: True on success
        """
        if not room_obj:
            raise Exception("You must pass a valid Room to delete_room")
        result = self._client.call(
            f'/configuration/v1/conference/{room_obj.id}/', 'DELETE')
        return result.status_code == 204

    def room_exists(self, room_obj: Room = None):
        """
        Check if a room actually exists.
        This probably duplicates get_room() a bit, but allows to verify this
        status without try/except block if so desired

        :param room_obj: Room to check
        :return: True on success
        """
        if not room_obj:
            raise Exception("You must pass a valid Room to room_exists")
        result = self._client.call(
            f'/configuration/v1/conference/{room_obj.id}/',
            'GET')
        if result.status_code == 200:
            return True
        return False

    def get_room(self, id: int = None, name: str = None):
        """
        Retrieve room details given id or name.
        If both are present, only ID will be used.
        Raises exception if not found.

        :param id: int
        :param name: str
        :return: Room object
        :raises: RoomDoesNotExist if room not found
        """

        if not (id or name):
            return Exception("invalid parameters for get_room, "
                             "you must pass either id or name")
        props = None
        if id is not None:
            result = self._client.call(f'/configuration/v1/conference/{id}/',
                                       'GET')
            if result.status_code == 200:
                props = result.json()
        elif name is not None:
            result = self._client.call(f'/configuration/v1/conference/',
                                       'GET',
                                       {'name': name})
            if result.status_code == 200 and len(result.json()['objects']) > 0:
                props = result.json()['objects'][0]

        if props:
            return Room(name=props['name'], id=int(props['id']), props=props)

        raise RoomDoesNotExist()
