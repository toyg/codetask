
Configuration API
=====

Authentication is via http basic auth.

The api is REST-based, i.e. using proper HTTP verbs for state management. GET calls will return current values.
Responses are paginated via `limit` and `offset` parameters, with a `total_count` indicating total number of pages.
Actual values are returned as the `objects` collection.
